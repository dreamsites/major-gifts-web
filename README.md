# Major-gifts-web
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/7430dc117ba14d1bab11a6976f53dd79)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=dreamsites/major-gifts-web&amp;utm_campaign=Badge_Grade)
## Начальная установка
```
npm install
```

### Live-сервер
```
npm run serve
```

### Сборка для сервера
```
npm run build
```

### Исправление ошибок в файлах
```
npm run lint
```