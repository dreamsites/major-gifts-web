import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// Initial state
const state = {
  isMenuOpened: false
};

// Getters
const getters = {
  isMenuOpened() {
    return state.isMenuOpened;
  }
};

// Mutations
const mutations = {
  toggleMenuOpened() {
    state.isMenuOpened = !state.isMenuOpened;
  }
};

// Actions
const actions = {
  triggerMenuOpened(context) {
    context.commit("toggleMenuOpened");
  }
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
});
